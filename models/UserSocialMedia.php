<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_social_media".
 *
 * @property string $id
 * @property string $social_media
 * @property string $username
 * @property int $user_id
 * @property int $created_at
 * @property int $updated_at
 */
class UserSocialMedia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_social_media';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'social_media', 'username', 'user_id', 'created_at', 'updated_at'], 'required'],
            [['social_media'], 'string'],
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['id', 'username'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'social_media' => 'Social Media',
            'username' => 'Username',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
