<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "book".
 *
 * @property int $id
 * @property string $title
 * @property string $author
 * @property string $publisher
 * @property float $price
 * @property int $stock
 */
class Auth extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auth';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['module', 'controller', 'action', 'user_id'], 'required'],
            [['module','controller','action'], 'string', 'max' => 50],
            [['user_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'module' => 'Module',
            'controller' => 'Controller',
            'action' => 'Action',
            'user_id' => 'ID User',
        ];
    }
}
