<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "book".
 *
 * @property int $id
 * @property string $title
 * @property string $author
 * @property string $publisher
 * @property float $price
 * @property int $stock
 */
class Book extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'book';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'author', 'publisher', 'price', 'stock'], 'required'],
            [['price'], 'number'],
            [['stock'], 'integer'],
            [['title'], 'string', 'max' => 150],
            [['author', 'publisher'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'author' => 'Author',
            'publisher' => 'Publisher',
            'price' => 'Price',
            'stock' => 'Stock',
        ];
    }
}
