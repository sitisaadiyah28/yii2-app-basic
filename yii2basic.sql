-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 13 Jan 2021 pada 11.01
-- Versi Server: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yii2basic`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `auth`
--

CREATE TABLE `auth` (
  `module` varchar(60) NOT NULL,
  `controller` varchar(60) NOT NULL,
  `action` varchar(60) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `auth`
--

INSERT INTO `auth` (`module`, `controller`, `action`, `user_id`) VALUES
('basic', 'category', 'create', 21),
('basic', 'category', 'index', 21),
('basic', 'ajax', 'book', 19),
('basic', 'ajax', 'book', 21),
('basic', 'ajax', 'get-book', 21),
('basic', 'ajax', 'depdrop', 21),
('basic', 'ajax', 'get-cities', 21),
('basic', 'auth', 'index', 21),
('basic', 'auth', 'view', 21),
('basic', 'auth', 'create', 21),
('basic', 'auth', 'update', 21),
('basic', 'auth', 'delete', 21),
('basic', 'auth', 'process-auth', 21),
('basic', 'book', 'index', 21),
('basic', 'book', 'create', 21),
('basic', 'book', 'update', 21),
('basic', 'book', 'delete', 21),
('basic', 'category', 'check', 21),
('basic', 'category', 'view', 21),
('basic', 'category', 'update', 21),
('basic', 'category', 'delete', 21),
('basic', 'employee', 'index', 21),
('basic', 'employee', 'view', 21),
('basic', 'employee', 'create', 21),
('basic', 'employee', 'update', 21),
('basic', 'employee', 'delete', 21),
('basic', 'post', 'index', 21),
('basic', 'post', 'view', 21),
('basic', 'post', 'create', 21),
('basic', 'post', 'update', 21),
('basic', 'post', 'delete', 21),
('basic', 'site', 'error', 21),
('basic', 'site', 'captcha', 21),
('basic', 'site', 'auth', 21),
('basic', 'category', 'import', 21),
('basic', 'book', 'view', 21),
('basic', 'category', 'export-excel', 21),
('basic', 'category', 'export-excel2', 21),
('basic', 'category', 'export-word', 21),
('basic', 'category', 'export-pdf', 21),
('gii', 'gii/default', 'action', 21),
('gii', 'gii/default', 'diff', 21),
('gii', 'gii/default', 'preview', 21),
('gii', 'gii/default', 'view', 21),
('gii', 'gii/default', 'index', 21),
('debug', 'debug/user', 'reset-identity', 21),
('debug', 'debug/user', 'set-identity', 21),
('debug', 'debug/default', 'download-mail', 21),
('debug', 'debug/default', 'toolbar', 21),
('debug', 'debug/default', 'view', 21),
('debug', 'debug/default', 'index', 21),
('debug', 'debug/default', 'db-explain', 21),
('admin', 'admin/post', 'delete', 21),
('admin', 'admin/post', 'update', 21),
('admin', 'admin/post', 'create', 21),
('admin', 'admin/post', 'view', 21),
('admin', 'admin/post', 'index', 21),
('basic', 'site', 'hello', 21),
('basic', 'site', 'tampil', 21),
('basic', 'site', 'url', 21),
('basic', 'site', 'hyperlink', 21),
('basic', 'site', 'komentar', 21),
('basic', 'site', 'active-record', 21),
('basic', 'site', 'query', 21),
('basic', 'site', 'index', 21),
('basic', 'site', 'login', 21),
('basic', 'site', 'logout', 21);

-- --------------------------------------------------------

--
-- Struktur dari tabel `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('Administrator', '21', 1610438317);

-- --------------------------------------------------------

--
-- Struktur dari tabel `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/*', 2, NULL, NULL, NULL, 1610438942, 1610438942),
('/admin/*', 2, NULL, NULL, NULL, 1610438943, 1610438943),
('/admin/book/*', 2, NULL, NULL, NULL, 1610439298, 1610439298),
('/admin/book/create', 2, NULL, NULL, NULL, 1610439288, 1610439288),
('/admin/book/delete', 2, NULL, NULL, NULL, 1610439302, 1610439302),
('/admin/book/index', 2, NULL, NULL, NULL, 1610439303, 1610439303),
('/admin/book/update', 2, NULL, NULL, NULL, 1610440024, 1610440024),
('/admin/book/view', 2, NULL, NULL, NULL, 1610440023, 1610440023),
('/admin/category/*', 2, NULL, NULL, NULL, 1610439298, 1610439298),
('/admin/category/create', 2, NULL, NULL, NULL, 1610440026, 1610440026),
('/admin/category/delete', 2, NULL, NULL, NULL, 1610440027, 1610440027),
('/admin/category/index', 2, NULL, NULL, NULL, 1610440028, 1610440028),
('/admin/category/update', 2, NULL, NULL, NULL, 1610440029, 1610440029),
('/admin/category/view', 2, NULL, NULL, NULL, 1610440029, 1610440029),
('/admin/default/*', 2, NULL, NULL, NULL, 1610439300, 1610439300),
('/admin/default/index', 2, NULL, NULL, NULL, 1610440031, 1610440031),
('/admin/employee/*', 2, NULL, NULL, NULL, 1610439308, 1610439308),
('/admin/employee/create', 2, NULL, NULL, NULL, 1610440032, 1610440032),
('/admin/employee/delete', 2, NULL, NULL, NULL, 1610440033, 1610440033),
('/admin/employee/index', 2, NULL, NULL, NULL, 1610440033, 1610440033),
('/admin/employee/update', 2, NULL, NULL, NULL, 1610440034, 1610440034),
('/admin/employee/view', 2, NULL, NULL, NULL, 1610440035, 1610440035),
('/admin/post/*', 2, NULL, NULL, NULL, 1610439309, 1610439309),
('/admin/post/create', 2, NULL, NULL, NULL, 1610440037, 1610440037),
('/admin/post/delete', 2, NULL, NULL, NULL, 1610440038, 1610440038),
('/admin/post/index', 2, NULL, NULL, NULL, 1610440039, 1610440039),
('/admin/post/update', 2, NULL, NULL, NULL, 1610440040, 1610440040),
('/admin/post/view', 2, NULL, NULL, NULL, 1610440040, 1610440040),
('/ajax/*', 2, NULL, NULL, NULL, 1610439310, 1610439310),
('/ajax/book', 2, NULL, NULL, NULL, 1610440042, 1610440042),
('/ajax/depdrop', 2, NULL, NULL, NULL, 1610440042, 1610440042),
('/ajax/get-book', 2, NULL, NULL, NULL, 1610440043, 1610440043),
('/ajax/get-cities', 2, NULL, NULL, NULL, 1610440044, 1610440044),
('/auth/*', 2, NULL, NULL, NULL, 1610439310, 1610439310),
('/auth/create', 2, NULL, NULL, NULL, 1610440045, 1610440045),
('/auth/delete', 2, NULL, NULL, NULL, 1610440046, 1610440046),
('/auth/index', 2, NULL, NULL, NULL, 1610440047, 1610440047),
('/auth/process-auth', 2, NULL, NULL, NULL, 1610440047, 1610440047),
('/auth/update', 2, NULL, NULL, NULL, 1610440048, 1610440048),
('/auth/view', 2, NULL, NULL, NULL, 1610440049, 1610440049),
('/book/*', 2, NULL, NULL, NULL, 1610439311, 1610439311),
('/book/create', 2, NULL, NULL, NULL, 1610440050, 1610440050),
('/book/delete', 2, NULL, NULL, NULL, 1610440051, 1610440051),
('/book/index', 2, NULL, NULL, NULL, 1610440051, 1610440051),
('/book/update', 2, NULL, NULL, NULL, 1610440052, 1610440052),
('/book/view', 2, NULL, NULL, NULL, 1610440053, 1610440053),
('/category/*', 2, NULL, NULL, NULL, 1610439311, 1610439311),
('/category/check', 2, NULL, NULL, NULL, 1610440054, 1610440054),
('/category/create', 2, NULL, NULL, NULL, 1610440055, 1610440055),
('/category/delete', 2, NULL, NULL, NULL, 1610440058, 1610440058),
('/category/index', 2, NULL, NULL, NULL, 1610440059, 1610440059),
('/category/update', 2, NULL, NULL, NULL, 1610440061, 1610440061),
('/category/view', 2, NULL, NULL, NULL, 1610440060, 1610440060),
('/debug/*', 2, NULL, NULL, NULL, 1610439312, 1610439312),
('/debug/default/*', 2, NULL, NULL, NULL, 1610439313, 1610439313),
('/debug/user/*', 2, NULL, NULL, NULL, 1610439313, 1610439313),
('/employee/*', 2, NULL, NULL, NULL, 1610439314, 1610439314),
('/gii/*', 2, NULL, NULL, NULL, 1610439314, 1610439314),
('/gii/default/*', 2, NULL, NULL, NULL, 1610439315, 1610439315),
('/mimin/*', 2, NULL, NULL, NULL, 1610439315, 1610439315),
('/mimin/role/*', 2, NULL, NULL, NULL, 1610439316, 1610439316),
('/mimin/role/update', 2, NULL, NULL, NULL, 1610439282, 1610439282),
('/mimin/route/*', 2, NULL, NULL, NULL, 1610439316, 1610439316),
('/mimin/user/*', 2, NULL, NULL, NULL, 1610439317, 1610439317),
('/post/*', 2, NULL, NULL, NULL, 1610439317, 1610439317),
('/site/*', 2, NULL, NULL, NULL, 1610439318, 1610439318),
('/site/index', 2, NULL, NULL, NULL, 1610439319, 1610439319),
('/test/*', 2, NULL, NULL, NULL, 1610439319, 1610439319),
('Administrator', 1, NULL, NULL, NULL, 1610438232, 1610438232);

-- --------------------------------------------------------

--
-- Struktur dari tabel `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('Administrator', '/*'),
('Administrator', '/admin/*'),
('Administrator', '/admin/book/*'),
('Administrator', '/admin/book/create'),
('Administrator', '/admin/book/delete'),
('Administrator', '/admin/book/index'),
('Administrator', '/admin/book/update'),
('Administrator', '/admin/book/view'),
('Administrator', '/admin/category/*'),
('Administrator', '/admin/category/create'),
('Administrator', '/admin/category/delete'),
('Administrator', '/admin/category/index'),
('Administrator', '/admin/category/update'),
('Administrator', '/admin/category/view'),
('Administrator', '/admin/default/*'),
('Administrator', '/admin/default/index'),
('Administrator', '/admin/employee/*'),
('Administrator', '/admin/employee/create'),
('Administrator', '/admin/employee/delete'),
('Administrator', '/admin/employee/index'),
('Administrator', '/admin/employee/update'),
('Administrator', '/admin/employee/view'),
('Administrator', '/admin/post/*'),
('Administrator', '/admin/post/create'),
('Administrator', '/admin/post/delete'),
('Administrator', '/admin/post/index'),
('Administrator', '/admin/post/update'),
('Administrator', '/admin/post/view'),
('Administrator', '/ajax/*'),
('Administrator', '/ajax/book'),
('Administrator', '/ajax/depdrop'),
('Administrator', '/ajax/get-book'),
('Administrator', '/ajax/get-cities'),
('Administrator', '/auth/*'),
('Administrator', '/auth/create'),
('Administrator', '/auth/delete'),
('Administrator', '/auth/index'),
('Administrator', '/auth/process-auth'),
('Administrator', '/auth/update'),
('Administrator', '/auth/view'),
('Administrator', '/book/*'),
('Administrator', '/book/create'),
('Administrator', '/book/delete'),
('Administrator', '/book/index'),
('Administrator', '/book/update'),
('Administrator', '/book/view'),
('Administrator', '/category/*'),
('Administrator', '/category/check'),
('Administrator', '/category/create'),
('Administrator', '/category/delete'),
('Administrator', '/category/index'),
('Administrator', '/category/update'),
('Administrator', '/category/view'),
('Administrator', '/debug/*'),
('Administrator', '/debug/default/*'),
('Administrator', '/debug/user/*'),
('Administrator', '/employee/*'),
('Administrator', '/gii/*'),
('Administrator', '/gii/default/*'),
('Administrator', '/mimin/*'),
('Administrator', '/mimin/role/*'),
('Administrator', '/mimin/route/*'),
('Administrator', '/mimin/user/*'),
('Administrator', '/post/*'),
('Administrator', '/site/*'),
('Administrator', '/site/index'),
('Administrator', '/test/*');

-- --------------------------------------------------------

--
-- Struktur dari tabel `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `book`
--

CREATE TABLE `book` (
  `id` int(11) NOT NULL,
  `title` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `publisher` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `stock` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `book`
--

INSERT INTO `book` (`id`, `title`, `author`, `publisher`, `price`, `stock`) VALUES
(1, 'Pemrograman PHP', 'Anton', 'Elexmedia', '65000.00', 5),
(2, 'Belajar AngularJs', 'Budi', 'Digipub', '40000.00', 3),
(3, 'Membuat Website Menggunakan Yii', 'Dira', 'Andi Offset', '50000.00', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `category`
--

INSERT INTO `category` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(27, 'Pendidikan  c', '2021-01-07 09:34:36', '0000-00-00 00:00:00'),
(32, 'Hewan Buas 2', '2021-01-07 06:31:46', '0000-00-00 00:00:00'),
(33, 'Tumbuhan', '2021-01-07 02:23:29', '0000-00-00 00:00:00'),
(34, 'Boneka Baru 2 barbie', '2021-01-07 05:10:42', '0000-00-00 00:00:00'),
(41, 'Oke 2', '2021-01-07 05:41:27', '0000-00-00 00:00:00'),
(42, 'Pendidikan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 'Makanananaaaa', '2021-01-07 08:23:52', '0000-00-00 00:00:00'),
(51, 'Teknologi Pendidikan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 'Testing Edit 4', '2021-01-07 08:36:29', '0000-00-00 00:00:00'),
(65, 'Teknologi Industri', '2021-01-07 08:42:32', '0000-00-00 00:00:00'),
(66, 'sosial', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 'sosial', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 'Budaya Minang 2', '2021-01-08 05:25:48', '0000-00-00 00:00:00'),
(70, 'Olaah', '2021-01-08 08:06:21', '0000-00-00 00:00:00'),
(71, 'Olaahsahahahaha', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 'samsa s.a', '2016-10-15 21:00:00', '2016-10-15 21:00:00'),
(73, 'samsa s.a', '2016-10-15 21:00:00', '2016-10-15 21:00:00'),
(74, 'Ayu Kategori111', '2021-01-11 02:12:29', '0000-00-00 00:00:00'),
(75, 'Ayu Kategori', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 'Testing', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 'Testing', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 'Pendidikan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 'Pemrograman Web', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 'Desain Grafis', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 'Aplikasi Perkantorn', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 'Pemrograman Mobile', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 'Jaringan Komputer', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 'Sistem Informasi', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `type` enum('kabupaten','kota') NOT NULL,
  `postal_code` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `city`
--

INSERT INTO `city` (`id`, `province_id`, `name`, `type`, `postal_code`) VALUES
(14, 1, 'Padang', 'kota', '25121'),
(15, 1, 'Bukittinggi', 'kota', '25124'),
(16, 3, 'Lubuk Linggau', 'kota', '12346'),
(17, 3, 'Pagar Alam', 'kota', '23414'),
(18, 2, 'Medan', 'kota', '98720'),
(19, 2, 'Sibolga', 'kabupaten', '34521');

-- --------------------------------------------------------

--
-- Struktur dari tabel `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `age` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `employee`
--

INSERT INTO `employee` (`id`, `name`, `age`) VALUES
(1, 'Siti', 22),
(3, 'Dora', 22),
(4, 'Bunayya', 26),
(5, 'arsyad', 18),
(6, 'Irfan aa', 23),
(7, 'Adliwal Ihsan', 24);

-- --------------------------------------------------------

--
-- Struktur dari tabel `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gallery`
--

INSERT INTO `gallery` (`id`, `image`) VALUES
(1, '1515604579.jpg'),
(2, 'aquarium-dengan-lampu-menerangi-air-penuh-ikan-768x484.jpg'),
(3, 'cupang.jpg'),
(4, 'forsipol.png'),
(5, 'Ikan-Guppy.jpg'),
(6, 'oauth-2-sm.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1609830235),
('m140506_102106_rbac_init', 1610434693),
('m151024_072453_create_route_table', 1610434794),
('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1610434693),
('m180523_151638_rbac_updates_indexes_without_prefix', 1610434694),
('m200409_110543_rbac_update_mssql_trigger', 1610434694),
('m210105_065745_create_book_table', 1609831315),
('m210105_072350_create_category_table', 1609831874),
('m210108_082506_create_post_table', 1610094358);

-- --------------------------------------------------------

--
-- Struktur dari tabel `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `body` text,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `province`
--

CREATE TABLE `province` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `province`
--

INSERT INTO `province` (`id`, `name`) VALUES
(1, 'Sumatera Barat'),
(2, 'Sumatera Utara'),
(3, 'Sumatera Selatan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `route`
--

CREATE TABLE `route` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `route`
--

INSERT INTO `route` (`name`, `alias`, `type`, `status`) VALUES
('/*', '*', '', 1),
('/admin/*', '*', 'admin', 1),
('/admin/book/*', '*', 'admin/book', 1),
('/admin/book/create', 'create', 'admin/book', 1),
('/admin/book/delete', 'delete', 'admin/book', 1),
('/admin/book/index', 'index', 'admin/book', 1),
('/admin/book/update', 'update', 'admin/book', 1),
('/admin/book/view', 'view', 'admin/book', 1),
('/admin/category/*', '*', 'admin/category', 1),
('/admin/category/create', 'create', 'admin/category', 1),
('/admin/category/delete', 'delete', 'admin/category', 1),
('/admin/category/index', 'index', 'admin/category', 1),
('/admin/category/update', 'update', 'admin/category', 1),
('/admin/category/view', 'view', 'admin/category', 1),
('/admin/default/*', '*', 'admin/default', 1),
('/admin/default/index', 'index', 'admin/default', 1),
('/admin/employee/*', '*', 'admin/employee', 1),
('/admin/employee/create', 'create', 'admin/employee', 1),
('/admin/employee/delete', 'delete', 'admin/employee', 1),
('/admin/employee/index', 'index', 'admin/employee', 1),
('/admin/employee/update', 'update', 'admin/employee', 1),
('/admin/employee/view', 'view', 'admin/employee', 1),
('/admin/post/*', '*', 'admin/post', 1),
('/admin/post/create', 'create', 'admin/post', 1),
('/admin/post/delete', 'delete', 'admin/post', 1),
('/admin/post/index', 'index', 'admin/post', 1),
('/admin/post/update', 'update', 'admin/post', 1),
('/admin/post/view', 'view', 'admin/post', 1),
('/ajax/*', '*', 'ajax', 1),
('/ajax/book', 'book', 'ajax', 1),
('/ajax/depdrop', 'depdrop', 'ajax', 1),
('/ajax/get-book', 'get-book', 'ajax', 1),
('/ajax/get-cities', 'get-cities', 'ajax', 1),
('/auth/*', '*', 'auth', 1),
('/auth/create', 'create', 'auth', 1),
('/auth/delete', 'delete', 'auth', 1),
('/auth/index', 'index', 'auth', 1),
('/auth/process-auth', 'process-auth', 'auth', 1),
('/auth/update', 'update', 'auth', 1),
('/auth/view', 'view', 'auth', 1),
('/book/*', '*', 'book', 1),
('/book/create', 'create', 'book', 1),
('/book/delete', 'delete', 'book', 1),
('/book/index', 'index', 'book', 1),
('/book/update', 'update', 'book', 1),
('/book/view', 'view', 'book', 1),
('/category/*', '*', 'category', 1),
('/category/check', 'check', 'category', 1),
('/category/create', 'create', 'category', 1),
('/category/delete', 'delete', 'category', 1),
('/category/index', 'index', 'category', 1),
('/category/update', 'update', 'category', 1),
('/category/view', 'view', 'category', 1),
('/debug/*', '*', 'debug', 1),
('/debug/default/*', '*', 'debug/default', 1),
('/debug/default/db-explain', 'db-explain', 'debug/default', 1),
('/debug/default/download-mail', 'download-mail', 'debug/default', 1),
('/debug/default/index', 'index', 'debug/default', 1),
('/debug/default/toolbar', 'toolbar', 'debug/default', 1),
('/debug/default/view', 'view', 'debug/default', 1),
('/debug/user/*', '*', 'debug/user', 1),
('/debug/user/reset-identity', 'reset-identity', 'debug/user', 1),
('/debug/user/set-identity', 'set-identity', 'debug/user', 1),
('/employee/*', '*', 'employee', 1),
('/employee/create', 'create', 'employee', 1),
('/employee/delete', 'delete', 'employee', 1),
('/employee/index', 'index', 'employee', 1),
('/employee/update', 'update', 'employee', 1),
('/employee/view', 'view', 'employee', 1),
('/gii/*', '*', 'gii', 1),
('/gii/default/*', '*', 'gii/default', 1),
('/gii/default/action', 'action', 'gii/default', 1),
('/gii/default/diff', 'diff', 'gii/default', 1),
('/gii/default/index', 'index', 'gii/default', 1),
('/gii/default/preview', 'preview', 'gii/default', 1),
('/gii/default/view', 'view', 'gii/default', 1),
('/mimin/*', '*', 'mimin', 1),
('/mimin/role/*', '*', 'mimin/role', 1),
('/mimin/role/create', 'create', 'mimin/role', 1),
('/mimin/role/delete', 'delete', 'mimin/role', 1),
('/mimin/role/index', 'index', 'mimin/role', 1),
('/mimin/role/permission', 'permission', 'mimin/role', 1),
('/mimin/role/update', 'update', 'mimin/role', 1),
('/mimin/role/view', 'view', 'mimin/role', 1),
('/mimin/route/*', '*', 'mimin/route', 1),
('/mimin/route/create', 'create', 'mimin/route', 1),
('/mimin/route/delete', 'delete', 'mimin/route', 1),
('/mimin/route/generate', 'generate', 'mimin/route', 1),
('/mimin/route/index', 'index', 'mimin/route', 1),
('/mimin/route/update', 'update', 'mimin/route', 1),
('/mimin/route/view', 'view', 'mimin/route', 1),
('/mimin/user/*', '*', 'mimin/user', 1),
('/mimin/user/create', 'create', 'mimin/user', 1),
('/mimin/user/delete', 'delete', 'mimin/user', 1),
('/mimin/user/index', 'index', 'mimin/user', 1),
('/mimin/user/update', 'update', 'mimin/user', 1),
('/mimin/user/view', 'view', 'mimin/user', 1),
('/post/*', '*', 'post', 1),
('/post/create', 'create', 'post', 1),
('/post/delete', 'delete', 'post', 1),
('/post/index', 'index', 'post', 1),
('/post/update', 'update', 'post', 1),
('/post/view', 'view', 'post', 1),
('/site/*', '*', 'site', 1),
('/site/about', 'about', 'site', 1),
('/site/active-record', 'active-record', 'site', 1),
('/site/auth', 'auth', 'site', 1),
('/site/captcha', 'captcha', 'site', 1),
('/site/contact', 'contact', 'site', 1),
('/site/error', 'error', 'site', 1),
('/site/hello', 'hello', 'site', 1),
('/site/hyperlink', 'hyperlink', 'site', 1),
('/site/index', 'index', 'site', 1),
('/site/komentar', 'komentar', 'site', 1),
('/site/login', 'login', 'site', 1),
('/site/logout', 'logout', 'site', 1),
('/site/query', 'query', 'site', 1),
('/site/signup', 'signup', 'site', 1),
('/site/tampil', 'tampil', 'site', 1),
('/site/url', 'url', 'site', 1),
('/test/*', '*', 'test', 1),
('/test/blog', 'blog', 'test', 1),
('/test/login', 'login', 'test', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(225) NOT NULL,
  `password_reset_token` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(19, 'sitisaadiyahap16@gmail.com', 'G3bpiF6mPOAccxrdVUQyPmuc6kWrzD4s', '$2y$13$ZFkDetkgztPbDry3NohHiecSTUt3mTFoAcFjZDbym.9tgilkGNhDu', 'FQBj6v2k_PrN8YTT94Jz9L8KluEJCCXC_1610349025', 'sitisaadiyahap16@gmail.com', 10, 1610349025, 1610349025),
(21, 'admin', 'G3bpiF6mPOAccxrdVUQyPmuc6kWrzD4s', '$2y$13$anq.roGS0UkE8KMdegM65eoiekruIbIMktKyqsDB791jQYqjVMXai', '', 'admin@gmail.com', 10, 1610354988, 1610354988),
(22, 'siti', 'iy2W9RVkUTVR3y0lWiUAPuEKy7aV2B4D', '$2y$13$Fpz65fLcFjzOyc.6vXiM2.d2ZfTpBm0nsaXgZsD3StjcGAVM2UQX.', '', 'sdyh@gmail.com', 10, 1610529293, 1610529293);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_photo`
--

CREATE TABLE `user_photo` (
  `user_id` int(11) NOT NULL,
  `photo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_photo`
--

INSERT INTO `user_photo` (`user_id`, `photo`) VALUES
(21, 'cupang.jpg'),
(22, 'Ikan-Guppy.jpg'),
(23, 'oauth-2-sm.png'),
(24, 'forsipol.png'),
(25, 'aquarium-dengan-lampu-menerangi-air-penuh-ikan-768x484.jpg'),
(26, 'kisspng-suit-formal-wear-clothing-dress-photo-template-5a74183e5b4781.6686155815175578223739.png'),
(27, 'Ikan-Guppy.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_social_media`
--

CREATE TABLE `user_social_media` (
  `id` varchar(255) NOT NULL,
  `social_media` enum('facebook','google','twitter','github') NOT NULL,
  `username` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_social_media`
--

INSERT INTO `user_social_media` (`id`, `social_media`, `username`, `user_id`, `created_at`, `updated_at`) VALUES
('', 'google', 'sitisaadiyahap16@gmail.com', 16, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `idx-auth_assignment-user_id` (`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`),
  ADD KEY `province_id` (`province_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-post-category_id` (`category_id`);

--
-- Indexes for table `province`
--
ALTER TABLE `province`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `route`
--
ALTER TABLE `route`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_photo`
--
ALTER TABLE `user_photo`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_social_media`
--
ALTER TABLE `user_social_media`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `province`
--
ALTER TABLE `province`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `user_photo`
--
ALTER TABLE `user_photo`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `city_ibfk_1` FOREIGN KEY (`province_id`) REFERENCES `province` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `fk-post-category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
