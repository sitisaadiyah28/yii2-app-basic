<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<h1>Gallery</h1>
<?php $form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data']
]) ?>
    <?= $form->field($model, 'image[]')->fileInput(['multiple' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>

    
<?php
ActiveForm::end();


    	foreach($model3 as $file){
		    echo Html::img(Yii::getAlias('@web').'/uploads/'.$file->image,[
		        'class'=>'img-thumbnail','style'=>'float:left;width:150px;'
		    ]);
		}
?>