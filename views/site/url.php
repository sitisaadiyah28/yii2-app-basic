<?php

use \yii\helpers\Url;

//URL ke HOME atu Web Index

echo Url::home();
echo '<br>';

//URL ke current Controller
echo Url::to(); 
echo '<br>';

//URL ke current Controller pada Action create
echo Url::to(['create']); echo '<br>';

//URL ke person controller pada action index
echo Url::to(['person/index']); echo '<br>';

//URL ke current Controller pada action index
//dengan parameter nama yang bernilai Siti
echo Url::to(['person/index','nama'=>'Siti']);