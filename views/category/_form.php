<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
Pjax::begin([
    'id'=>'pjax-form','timeout'=>false,
]);
/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

    <?php
        if(Yii::$app->request->isAjax)
            echo \app\widgets\Alert::widget();
    ?>

    <?php $form = ActiveForm::begin([
            'options' => ['data-pjax' =>true]
        ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
    
       <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>

        <?= Html::a('Back', ['index'], ['class' => 'btn btn-success',
            'onclick'=>'
          $("#categoryModal").modal("hide");
          return false;
        '
        ])?>
    </div>

    <?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>

