<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
?>
    

    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'username',
    ],
    ]) ?>


    <table class="table table-striped">
        <tr>
            <th>Modules</th>
            <th>Controllers</th>
            <th>Actions</th>
            <th>Auth</th>
        </tr>
        <?php
            foreach($routes as $row) {
                ?>
                <tr >
                    <td><?= $row['module']?></td >
                    <td><?= $row['controller']?></td >
                    <td><?= $row['action']?></td >
                    <td><?= Html::checkbox('auth[]',$row['auth'], [
                        'class' => 'processAuth',
                        'data-module' => $row['module'],
                        'data-controller' => $row['controller'],
                        'data-action' => $row['action'],
                        ]); ?></td >
                </tr >
                <?php
            }
        ?>
    </table>

    <?php
    $this->registerJs('
        $(".processAuth").on("click", function (e) {
            module = $(this).attr("data-module");
            controller = $(this).attr("data-controller");
            action = $(this).attr("data-action");
            user_id = '.$model->id.';
            checked = $(this).prop("checked");
            var link = "'.Url::to(['process-auth']).'?module="+module+
                "&controller="+controller+"&action="+action+"&user_id="+user_id+
                "&checked="+checked;
            $.get(link, function(data) {
                alert(data)
            });
        });
    ');

