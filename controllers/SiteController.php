<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\user;
use app\models\ContactForm;
use app\models\UserSocialMedia;

class SiteController extends Controller
{
    public function actionHello($nama="")
    {
        return "Hello ". $nama;
    }

    public function actionTampil($nama="")
    {
        return $this->render('hello',[
            'nama' => $nama,
        ]);
    }
    public function actionUrl()
    {
        return $this->render('url');
    }
    public function actionHyperlink()
    {
        return $this->render('hyperlink');
    }
    public function actionKomentar()
    {
        $model = new \app\models\Komentar();

        //Jika form disubmit dengan method POST
        if(Yii::$app->request->post()){
            $model->load(Yii::$app->request->post());
            if($model->validate())
            {
                Yii::$app->session->setFlash('success','Terima Kasih');
            }
            else{
                Yii::$app->session->seFlash('error','Maaf, Salah!!');
            }
            return $this->render('hasil_komentar', [
                'model' => $model,
            ]);
        }
        else{
            return $this->render('komentar', [
            'model' => $model,
            ]);
        }
        
    }
    public function actionActiveRecord()
    {
        $employees = \app\models\Employee::find()->all();
        foreach($employees as $employee){
            echo "<br>";
            echo $employee->id." ";
            echo $employee->name." ";
            echo "(".$employee->age.") ";            
        }
    }


    public function actionQuery()
    {
        $db = Yii::$app->db;
        $command = $db->createCommand('SELECT * FROM employee');
        $employees =  $command->queryAll();
        //Ekstrak Data
        foreach ($employees as $employee) {
            echo "<br>";
            echo $employee['id']." ";
            echo $employee['name']." ";
            echo "(".$employee['age'].")";
            echo "<hr>";
            // return a single column (the first column)
            $names = $db->createCommand('SELECT name FROM employee')->queryColumn();
            print_r($names);
            echo "<hr>";
            // return a scalar
            $count = $db->createCommand('SELECT COUNT(*) FROM employee')->queryScalar();
            echo "Jumlah employee ".$count;
            echo "<hr>";

        }
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successCallback'],
            ],
        ];
    }

    public function successCallback($client)
    {
       //call safeAttributes method for properly format data
       $attributes = $this->safeAttributes($client);

       //find data social media in database
       $user_social_media = UserSocialMedia::find()->where([
            'social_media' => $attributes['social_media'],
            'id'=>(string)$attributes['id'],
            'username'=>$attributes['username'],
       ])
       ->one();

       //if data found

       if($user_social_media)
       {
            if($user_social_media){
                //get user from ralation
                $user = $user_social_media->user;
                //check user is active
                if($user->status==User::STATUS_ACTIVE)
                {
                    //do automatic login
                    Yii::$app->user->login($user);
                }
                else
                {
                    Yii::$app->session->setFlash('error','Login gagal, status user tidak aktif');
                }

            }
       }
       else
       {
        //if data not found
            //check if email sosial media exits in tabel user
            $user = User::find()->where(['email' => $attributes['email']])->one();

            //if user found
            if($user){
                //check user is active
                if($user->status==User::STATUS_ACTIVE)
                {
                    // add to table user social media
                    $user_social_media = new UserSocialMedia([
                        'social_media' => $attributes['social_media'],
                        'id'=>(string)$attributes['username'],
                        'username'=>$attributes['username'],
                        'user_id'=>$user->id,
                    ]);
                    $user_social_media->save();

                    //do automatic login
                    Yii::$app->user->login($user);
                }
                else
                {
                    Yii::$app->session->setFlash('error','Login gagal, status user tidak aktif');
                }
            }
            else{
                //check if social media not twitter
                if($attributes['social_media']!='twitter'){
                    //do auctomatic signup
                    $password = Yii::$app->security->generateRandomString(6);
                    $user = new USer([
                        'username' => $attributes['username'],
                        'email' => $attributes['email'],
                        'password' => $password,
                    ]);
                    $user->generateAuthKey();
                    $user->generatePasswordResetToken();
                    if($user->save()){
                        $user_social_media = new UserSocialMedia([
                            'social_media' => $attributes['social_media'],
                            'id'=>(string)$attributes['id'],
                            'username'=>$attributes['username'],
                            'user_id' =>$user->id,
                        ]);
                        $user_social_media->save();
                        //do automatic login
                        Yii::$app->user->login($user);
                    }
                    else{
                        Yii::$app->session->setFlash('error','Login gagal, galat saat registrasi');
                    }                   
                }
                else{
                        //save data attributes to session
                        $session = Yii::$app->session;
                        $session['attributes']=$attributes;

                        //redirect to signup, via property successUrl
                        $this->action->successUrl = Url::to(['signup']);
                    }
            }
       }
    }

    public function safeAttributes($client){
         //get user data form client
        $attributes = $client->getUserAttributes();
        /*print_r($attributes);
        exit;*/
        //user login or signup comes here

        // set default value
        $safe_attributes = [
            'social_media'=> '',        
            'id'=> '',        
            'username'=> '',        
            'name'=> '',        
            'email'=> '',        
        ];

        //get value from user attributes base on social media

        if($client instanceof \yii\authclient\clients\Google) {
            $safe_attributes = [
            'social_media'=> 'google',        
            'id'=> $attributes['id'],        
            'username'=> $attributes['email'],        
            'name'=> $attributes['name'],        
            'email'=> $attributes['email'],      
            ];
        }
        else if($client instanceof \yii\authclient\clients\GitHub)
        {
             $safe_attributes = [
            'social_media'=> 'github',        
            'id'=> $attributes['id'],        
            'username'=> $attributes['login'],        
            'name'=> $attributes['name'],        
            'email'=> $attributes['email'],        
             ];
        }
        return $safe_attributes;
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSignup()
    {
        $model = new \app\models\SignupForm();

        //use session
        $session = Yii::$app->session;
        $attributes = $session['attributes'];

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if($session->has('attributes')){
                    //add data user_social_media
                    $user_social_media = new UserSocialMedia([
                        'social_media' => $attributes['social_media'],
                        'id'=>(string)$ttributes['id'],
                        'username'=>$attributes['username'],
                        'user_id'=>$user->id,
                    ]);
                    $user_social_media->save();
                }


                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }
        if($session->has('attributes')){
            //set form field with data from social media
            $model->username = $attributes['username'];
            $model->email = $attributes['email'];
        }



        return $this->render('signup', [
            'model' => $model,
        ]);
    }


    public function actionUpload()
    {
        $user_id = \Yii::$app->user->id;
        $model = \app\models\UserPhoto::find()->where([
            'user_id' => $user_id
        ])->one();

        if(!$model){
            $model = new \app\models\UserPhoto([
                'user_id' => $user_id
            ]);
        }

        if(\Yii::$app->request->post()){
            $model->photo = \yii\web\UploadedFile::getInstance($model, 'photo');
            if($model->validate()){
                $saveTo = 'uploads/' . $model->photo->baseName . '.' . $model->photo->extension;
                if($model->photo->saveAs($saveTo)){
                    $model->save(false);
                    Yii::$app->session->setFlash('success','Foto berhasil diupload');
                }
            }
            
        }
        return $this->render('upload', [
            'model' => $model
        ]);
    }

    public function actionGallery()
    {
        $model = new \app\models\Gallery();
        $model3 = \app\models\Gallery::find()->all();
        if (\Yii::$app->request->post()) {
            $model->image = \yii\web\UploadedFile::getInstances($model, 'image');
            if ($model->validate()) {
                foreach ($model->image as $file) {
                    $saveTo = 'uploads/' . $file->baseName . '.' . $file->extension;
                    if ($file->saveAs($saveTo)) {
                        $model2 = new \app\models\Gallery([
                            'image' => $file->baseName . '.' . $file->extension,
                        ]);
                        $model2->save(false);
                    }
                }
                \Yii::$app->session->setFlash('success', 'Image berhasil di upload');
            }
        }

        return $this->render('gallery', [
            'model' => $model,
            'model3' => $model3,
        ]);
    }


    public function actionDownload($inline=false){
        $user_id = \Yii::$app->user->id;
        $model = \app\models\UserPhoto::find()->where([
            'user_id' => $user_id
        ])->one();
        $path = Yii::getAlias("@web") . '/uploads/';
        $file_download = $path . $model->photo;
        $fake_filename = $model->photo;
        $response = Yii::$app->getResponse();
        $response->sendFile($file_download, $fake_filename,['inline'=>$inline]);
    }



}
