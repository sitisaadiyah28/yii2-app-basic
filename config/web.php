<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'timeZone' => 'Asia/Jakarta',
    'modules' => [
        'mimin' => [
            'class' => '\hscstudio\mimin\Module',
        ],
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],
    ],
    'as access' => [
        'class' => '\hscstudio\mimin\components\AccessControl',
        'allowActions' => [
            // add wildcard allowed action here!
            'site/*',
            'debug/*',
            'mimin/*', // only in dev mode
        ],
    ],
    'bootstrap' => ['log'],
    // 'on beforeAction' => function($event){
    //     $action = $event->action;
    //     $moduleID = $action->controller->module->id;
    //     $controllerID = $action->controller->id;
    //     $actionID = $action->id;
    //     $user = \Yii::$app->user;
    //     $userID = $user->id;
    //     if(!in_array($controllerID,['default','site'])){
    //         $auth = \app\models\Auth::find()
    //             ->where([
    //                 'module' => $moduleID,
    //                 'controller' => $controllerID,
    //                 'action' => $actionID,
    //                 'user_id' => $userID,
    //             ])
    //             ->count();
    //         if($auth==0) {
    //             if (!$action instanceof \yii\web\ErrorAction) {
    //                 if ($user->getIsGuest()) {
    //                     $user->loginRequired();
    //                 } else {
    //                     throw new \yii\web\ForbiddenHttpException('Anda tidak diizinkan untuk mengakses halaman ' . $action->id . ' ini!');
    //                 }
    //             }
    //         }    
    //     }
    // },
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'google' =>[
                    'class' => 'yii\authclient\clients\Google',
                    'clientId' => '836656078125-5ed8mjadgj6q91gci3g15ji536bvt7ep.apps.googleusercontent.com',
                    'clientSecret' => 'QZrDq1UpFlV4EZ3pdDzZXnIQ',
                ],
                'github' =>[
                    'class' => 'yii\authclient\clients\GitHub',
                    'clientId' => '81b58aef8965e3eae948
',
                    'clientSecret' => 'ca6f0dc536cf4ba71ceb1cbecfca55cc043fe0bf',
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',    
        ],

        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'AJxQviINHpoWIDnEcS2fGAatJajClquG',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => false,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
